# ENIGMA M3 Wehrmacht und Luftwaffe

## Info
- Author:   Juho Frigård
- Version:  1.0
- Date: 30.03.2022

Replication of the Enimga encryption/decryption machine used by German 
Wehrmacht and Luftwaffe during World War II.

## Usage
Start Enigma with command.
```console
$ ./enigma
```

The use will be prompted for which Enigma operation to run.
```console
=======================================================
ENIGMA M3 Wehrmacht und Luftwaffe
Author:   Juho Frigård
Version:  1.1 (30.03.2022)


 Command   Action !
 -----------------------------------------------------
  0        Print this help text.
  1        Encrypt characters one by one.
  2        Encrypt pieces of text at once.
  3        Encrypt contents of a file.
  4        Reset Enigma machine.

[Enigma]: 
```

### Configuration File

The configuration file __enigma.conf__ contains the initial settings for Enigma. Further information about setup parameters:
- [Enigma World Code Group](https://www.enigmaworldcodegroup.com/how-to-create-an-enigma-message)
- [Wikipedia: Enigma Rotor Details](https://en.wikipedia.org/wiki/Enigma_rotor_details)

#### Example
```console
#------------------------------------------------------------------------------#
# This is a configuration file for Enigma M3 Wehrmacht und Luftwaffe           #
#                                                                              #
# 1. Choose rotor numbers from 1-5.                                            #
# 2. Set rotor positions from A-Z.                                             #
# 3. set ring positions from A-Z.                                              #
# 4. Choose reflector from 'B' or 'C'.                                         #
# 5. Set block size for output.                                                #
# 6. Set column size for output.                                               #
#                                                                              #
# Set all parameters in the format <parameter_name>=<value>                    #
# -----------------------------------------------------------------------------#

# Right rotor configuration
RIGHT_ROTOR=5
RIGHT_ROTOR_POSITION=y
RIGHT_RING_POSITION=F

# Middle rotor configuration
MIDDLE_ROTOR=3
MIDDLE_ROTOR_POSITION=G
MIDDLE_RING_POSITION=B

# Left rotor configuration
LEFT_ROTOR=1
LEFT_ROTOR_POSITION=Z
LEFT_RING_POSITION=Y

# Reflector can be either 'B' or 'C'.
REFLECTOR=B

# Cross-wiring configuration
CROSS_WIRING=ab,cd,ef,gh

# Block size
BLOCK_SIZE=5

# Column size
COLUMNS=5
```