"""
This is the Enigma machine used by German Wehrmacht and Luftwaffe before and 
during the second world war.

Usage:
import enigma_machine
enigma = enigma_machine.Enigma_machine(rotor_nums, ring_set, rot_set, refl_set, 
    cross_wir)

Parameters:
rotor_nums: Rotors to use (0-4) as a list (e.g. [0,1,2]).
ring_set: Settings for the outer ring (A-Z) as a list (e.g. ["A","A","A"]).
rot_set: Rotor position settings (A-Z) as a list (e.g. ["A","A","A"]).
refl_set: Reflector to be used (B or C).
cross_wir: Cross wiring table configuration given as a list of tuples 
    (e.g. [("A","B"),("C","D"),("E","F")])
"""

import enigma_rotor

ALPHABET = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P',
    'Q','R','S','T','U','V','W','X','Y','Z']
REFLECTOR_B = ['Y','R','U','H','Q','S','L','D','P','X','N','G','O','K','M','I',
    'E','B','F','Z','C','W','V','J','A','T']
REFLECTOR_C = ['F','V','P','J','I','A','O','Y','E','D','R','Z','X','W','G','C',
    'T','K','U','Q','S','B','N','M','H','L']


class Enigma_machine:
    def __init__(self, rotor_nums=[0, 1, 2], ring_set=["A","A","A"], 
        rot_set=["A","A","A"], refl_set="B", cross_wir=[]):
        
        self.cross_wiring = cross_wir
        self.rotors = []
        self.reflector = []
        self.rotor_nums = rotor_nums
        self.reflector_setting = refl_set.upper()
        self.ring_settings = ring_set
        self.rotor_settings = rot_set
        self.check_settings()
        self.set_initial_state()
        self.double_step = False
    
    def check_settings(self):
        if len(self.rotor_nums) != 3:
            raise ValueError("Invalid number of rotors ({}). There should be "\
                "3.".format(len(self.rotor_nums)))

        for value in self.rotor_nums:
            if type(value) is not int:
                raise TypeError("Invalid rotor number ({}). Rotor numbers must"\
                    " be integers.".format(value))
            if value < 0 or value > 4:
                raise ValueError("Invalid rotor selection.")
        
        if len(self.ring_settings) != 3:
            raise ValueError("Invalid ring settings.")
        
        for value in self.ring_settings:
            if value.upper() not in ALPHABET:
                raise TypeError("Ring setting must be a character in the "
                    "alphabet.")
        
        if len(self.rotor_settings) != 3:
            raise ValueError("Invalid rotor settings.")

        for value in self.rotor_settings:
            if value.upper() not in ALPHABET:
                raise TypeError("Rotor setting ({}) must be a character in " 
                    "the alphabet.".format(value))

        if self.reflector_setting != "B" and self.reflector_setting != "C":
            raise ValueError("reflector setting must be either 'B' or 'C'")
        
        all_cross_wirings = []
        for cw_tuple in self.cross_wiring:
            if type(cw_tuple) is not tuple:
                raise TypeError("Cross wiring must a list of tuples.")
            
            value1 = cw_tuple[0].upper()
            value2 = cw_tuple[-1].upper()
            
            if value1 not in ALPHABET or value2 not in ALPHABET:
                raise ValueError("Invalid cross wiring setting ({} -> {}).".
                    format(value1, value2))
            
            if value1 in all_cross_wirings:
                raise ValueError("Invalid cross wiring setting. Cannot cross " \
                    "wire the same character ({}) twice.".format(value1))

            if value2 in all_cross_wirings:
                raise ValueError("Invalid cross wiring setting. Cannot cross " \
                    "wire the same character ({}) twice.".format(value2))
            
            all_cross_wirings.append(value1)
            all_cross_wirings.append(value2)
    
    def cross_wiring_table(self, character):
        for cw_tuple in self.cross_wiring:
            value1 = cw_tuple[0].upper()
            value2 = cw_tuple[-1].upper()
            if character == value1:
                return value2
            elif character == value2:
                return value1
        return character

    def encrypt_character(self, character_raw):
        character_upper = character_raw.upper()
        character = self.cross_wiring_table(character_upper)

        if character not in ALPHABET:
            raise ValueError("Invalid input character '{}'.".format(character))

        self.turn_rotors()  # Rotors are turned BEFORE encrypting a character.
        
        # Current flows through the rotors from right to left.
        trans1 = (self.rotors[0]).encrypt_character(character)
        trans2 = (self.rotors[1]).encrypt_character(trans1)
        trans3 = (self.rotors[2]).encrypt_character(trans2)
        
        # The current flows through the reflector.
        trans_reflector = self.reflector[ALPHABET.index(trans3)]
        
        # Current flow through the same rotors but in the opposite direction.
        trans4 = (self.rotors[2]).encrypt_character_inv(trans_reflector)
        trans5 = (self.rotors[1]).encrypt_character_inv(trans4)
        result = (self.rotors[0]).encrypt_character_inv(trans5)

        return self.cross_wiring_table(result)
    
    def set_initial_state(self):
        self.rotors.append(enigma_rotor.Rotor(self.rotor_nums[0], 0,
            self.rotor_settings[0], self.ring_settings[0]))
        self.rotors.append(enigma_rotor.Rotor(self.rotor_nums[1], 1,
            self.rotor_settings[1], self.ring_settings[1]))
        self.rotors.append(enigma_rotor.Rotor(self.rotor_nums[2], 2,
            self.rotor_settings[2], self.ring_settings[2]))
        
        if self.reflector_setting == "B":
            self.reflector = REFLECTOR_B
        else:
            self.reflector = REFLECTOR_C

    def turn_rotors(self):
        rotate_next = self.rotors[0].rotate()
        
        if rotate_next or self.double_step:
            rotate_next = self.rotors[1].rotate()
            self.double_step = self.rotors[1].check_double_step()
        
        if rotate_next:
            self.rotors[2].rotate()
