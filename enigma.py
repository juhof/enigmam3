#! /usr/bin/python3

import sys
import os
import enigma_machine

HEADER = """\
=======================================================
ENIGMA M3 Wehrmacht und Luftwaffe
Author:   Juho Frigård
Version:  1.1 (30.03.2022)
"""

HELP = """\

 Command   Action !
 -----------------------------------------------------
  0        Print this help text.
  1        Encrypt characters one by one.
  2        Encrypt pieces of text at once.
  3        Encrypt contents of a file.
  4        Reset Enigma machine.
\
"""

ALLOWED_CHARS = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O',
                 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
DIRECTORY = dir_path = os.path.dirname(os.path.realpath(__file__))
CONF_FILE = DIRECTORY + "/enigma.conf"
ENIGMA = None
BLOCK_SIZE = 5
COLUMNS = 5


def check_character(character):
    if character in ALLOWED_CHARS and len(character) == 1:
        return True
    return False


def encryption():
    pass


def encrypt_continous():
    try:
        print("Insert characters one by one. Quit by pressing Ctrl+C.")
        while True:
            character = input("[input]: ").upper()
            if check_character(character):
                encrypted = ENIGMA.encrypt_character(character)
                print("             {}".format(encrypted))
            else:
                print("ERROR: Invalid character.")
    except KeyboardInterrupt:
        print()
        return


def encrypt_file():
    try:
        input_file_name_rel = input("[Input file name]: ")
        input_file_name = DIRECTORY + "/" + input_file_name_rel

        file_encryptor(input_file_name)

    except FileNotFoundError:
        print("ERROR: File '{}' not found.".format(input_file_name))
        return
    except KeyboardInterrupt:
        print()
        return


def file_encryptor(input_file_name):
    try:
        input_file = open(input_file_name, "r")
        input_file_name_split = input_file_name.split(".")
        input_file_name = input_file_name_split[0]
        output_file_name = input_file_name + "_enc.txt"
        output_file = open(output_file_name, "w+")
        blocks = 0
        letters = 0

        for line_raw in input_file:
            line = line_raw.replace("\n", "")
            for character in line:
                if character.upper() in ALLOWED_CHARS:
                    output_file.write(ENIGMA.encrypt_character(character))
                    letters += 1
                    if letters == BLOCK_SIZE:
                        letters = 0
                        output_file.write(" ")
                        blocks += 1
                    if blocks == COLUMNS:
                        blocks = 0
                        output_file.write("\n")

        print("Output written to file '{}'".format(output_file_name))

    except FileNotFoundError:
        print("ERROR: File '{}' not found.".format(input_file_name))
        return


def parse_cross_wiring(cross_wiring_raw):
    cross_wiring = []
    cross_wiring_list = cross_wiring_raw.split(",")
    for pair in cross_wiring_list:
        if len(pair) != 2:
            # Invalid configuration.
            print("ERROR: Invalid cross-wiring configuration ({}).".
                  format(pair))
            continue
        cross_wiring_tuple = (pair[0].upper(), pair[1].upper())
        cross_wiring.append(cross_wiring_tuple)

    return cross_wiring


def read_conf_file():
    try:
        conf_file = open(CONF_FILE, "r")

        rotors = [0, 0, 0]
        rotor_positions = ["", "", ""]
        ring_positions = ["", "", ""]
        reflector = ""
        cross_wiring = ""

        for line in conf_file:
            line = line.replace("\n", "")
            conf = line.split("=")
            if len(conf) != 2:
                pass
            elif line[0] == '#':
                pass
            elif conf[0] == "RIGHT_ROTOR":
                rotors[0] = int(conf[1]) - 1
            elif conf[0] == "MIDDLE_ROTOR":
                rotors[1] = int(conf[1]) - 1
            elif conf[0] == "LEFT_ROTOR":
                rotors[2] = int(conf[1]) - 1
            elif conf[0] == "RIGHT_ROTOR_POSITION":
                rotor_positions[0] = conf[1].upper()
            elif conf[0] == "MIDDLE_ROTOR_POSITION":
                rotor_positions[1] = conf[1].upper()
            elif conf[0] == "LEFT_ROTOR_POSITION":
                rotor_positions[2] = conf[1].upper()
            elif conf[0] == "RIGHT_RING_POSITION":
                ring_positions[0] = conf[1].upper()
            elif conf[0] == "MIDDLE_RING_POSITION":
                ring_positions[1] = conf[1].upper()
            elif conf[0] == "LEFT_RING_POSITION":
                ring_positions[2] = conf[1].upper()
            elif conf[0] == "REFLECTOR":
                reflector = conf[1].upper()
            elif conf[0] == "CROSS_WIRING":
                cross_wiring = parse_cross_wiring(conf[1])
            elif conf[0] == "BLOCK_SIZE":
                global BLOCK_SIZE
                BLOCK_SIZE = int(conf[1])
            elif conf[0] == "COLUMNS":
                global COLUMNS
                COLUMNS = int(conf[1])

        conf_file.close()
        return [rotors, ring_positions, rotor_positions, reflector,
                cross_wiring]

    except FileNotFoundError:
        print("ERROR: Configuration file '{}' not found.".format(CONF_FILE))
        exit()
    except TypeError as type_error:
        print("ERROR: {}".format(type_error))
        exit()


def set_enigma(conf):
    try:
        global ENIGMA
        ENIGMA = enigma_machine.Enigma_machine(conf[0], conf[1], conf[2],
                                               conf[3], conf[4])
    except ValueError as err:
        print("ERROR: {}".format(err))
        exit()
    except TypeError as err:
        print("ERROR: {}".format(err))
        exit()


def main():
    print(HEADER)
    print(HELP)
    configuration = read_conf_file()
    set_enigma(configuration)
    try:
        command = ""
        while command != "q":
            command = input("[Enigma]: ")
            if command == "0":
                print(HELP)
            elif command == "1":
                encrypt_continous()
            elif command == "2":
                encryption()
            elif command == "3":
                encrypt_file()
            elif command == "4":
                print("Resetting Enigma...", end='')
                set_enigma(configuration)
                print("  [OK]")
    except KeyboardInterrupt:
        print()


if __name__ == "__main__":
    main()
