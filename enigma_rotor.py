#! /usr/bin/python3


ALL_LETTERS = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P',
    'Q','R','S','T','U','V','W','X','Y','Z']
ROTOR_SEQUENCES = [['E','K','M','F','L','G','D','Q','V','Z','N','T','O','W','Y',
    'H','X','U','S','P','A','I','B','R','C','J'],
['A','J','D','K','S','I','R','U','X','B','L','H','W','T','M','C','Q','G','Z',
    'N','P','Y','F','V','O','E'],
['B','D','F','H','J','L','C','P','R','T','X','V','Z','N','Y','E','I','W','G',
    'A','K','M','U','S','Q','O'],
['E','S','O','V','P','Z','J','A','Y','Q','U','I','R','H','X','L','N','F','T',
    'G','K','D','C','M','W','B'],
['V','Z','B','R','G','I','T','Y','U','P','S','D','N','H','L','X','A','W','M',
    'J','Q','O','F','E','C','K']]

CORE = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R',
    'S','T','U','V','W','X','Y','Z']
RING = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R',
    'S','T','U','V','W','X','Y','Z']

# When these step notches are shown in the window the rotor to the left 
# of this rotor will also rotate. The real notches are in places Y, M, D, R, H.
# Memory rule: "Royal Flags Wave Kings Above"
STEP_NOTCHES = ["R","F","W","K","A"]

class Rotor:
    def __init__(self, rotor_seq, rotor_num, rotor_pos, ring_pos):
        rotor_pos = rotor_pos.upper()
        ring_pos = ring_pos.upper()

        self.rotor_sequence_num = rotor_seq
        self.step_notch = STEP_NOTCHES[rotor_seq]
        self.rotor_num = rotor_num
        self.rotor_sequence = ROTOR_SEQUENCES[rotor_seq]
        self.offset = 0
        self.ring_offset = 0
        self.ring = ALL_LETTERS
        self.core = ALL_LETTERS
        
        self.set_ring_position(ring_pos)
        self.set_rotor_position(rotor_pos)
        self.rotor_setting = rotor_pos
    
    def check_double_step_char(self):
        index = ALL_LETTERS.index(self.step_notch) - 1
        return ALL_LETTERS[index % 26]
        
    def check_double_step(self):
        double_step_char = self.check_double_step_char()
        if self.rotor_num == 1 and self.ring[0] == double_step_char:
            return True  # Double-step
        return False

    def encrypt_character(self, character):
        index = ALL_LETTERS.index(character)
        encrypted_char = self.rotor_sequence[index]
        encrypted_index = self.core.index(encrypted_char)
        return ALL_LETTERS[encrypted_index]
    
    def encrypt_character_inv(self, character):
        index = ALL_LETTERS.index(character)
        core_char = self.core[index]
        index_seq = self.rotor_sequence.index(core_char)
        return ALL_LETTERS[index_seq]
    
    def rotate(self):
        self.turn_rotor()
        self.turn_core()
        self.turn_ring()
        return self.rotate_next()
    
    def rotate_next(self):
        if self.ring[0] == self.step_notch:
            return True
        return False
    
    def set_ring_position(self, ring_position):
        while self.ring[0] != ring_position:
            self.turn_ring()
    
    def set_rotor_position(self, rotor_position):
        while self.ring[0] != rotor_position:
            self.rotate()
    
    def turn_core(self):
        new_core = []
        for character in self.core:
            if character != self.core[0]:
                new_core.append(character)
        new_core.append(self.core[0])
        self.core = new_core

    def turn_ring(self):
        new_ring = []
        for character in self.ring:
            if character != self.ring[0]:
                new_ring.append(character)
        new_ring.append(self.ring[0])
        self.ring = new_ring

    def turn_rotor(self):
        new_seq = []
        for character in self.rotor_sequence:
            if character != self.rotor_sequence[0]:
                new_seq.append(character)
        new_seq.append(self.rotor_sequence[0])
        self.rotor_sequence = new_seq
